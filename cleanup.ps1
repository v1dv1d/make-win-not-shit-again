Get-AppxPackage -AllUsers *Music* | Remove-AppxPackage

Get-AppxPackage -AllUsers *OneNote* | Remove-AppxPackage

Get-AppxPackage -AllUsers *messaging* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Maps* | Remove-AppxPackage

Get-AppxPackage -AllUsers *News* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Finance* | Remove-AppxPackage

Get-AppxPackage -AllUsers *solitairecollection* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Sport* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Sway* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Commsphone* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Windowsphone* | Remove-AppxPackage

Get-AppxPackage -AllUsers *Weather* | Remove-AppxPackage

Get-AppxPackage -AllUsers *getstarted* | Remove-AppxPackage

Get-AppxPackage -AllUsers *CandyCrushSaga* | Remove-AppxPackage

#Get-AppxPackage -AllUsers *Twitter* | Remove-AppxPackage

Get-AppxPackage -AllUsers Microsoft.windowscommunications | Remove-AppxPackage

taskkill /f /im OneDrive.exe

C:\Windows\SysWOW64\OneDriveSetup.exe /uninstall
