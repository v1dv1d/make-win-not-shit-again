# Make Windows not shit again

## Usage

These Scripts will make using Windows somewhat bearable. 

To be able to execute PowerShell-Scripts on a fresh 
Windows-Install you need to first execute:
```powershell
Set-ExecutionPolicy Unrestricted
````

Afterwards just run `.\cleanup.ps1` and you're good to go.
